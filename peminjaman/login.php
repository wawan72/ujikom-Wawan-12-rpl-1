<?php
session_start();
include"koneksi.php";

if(isset($_POST['user_log'])){
	$username = $_POST['username'];
	$password = $_POST['password'];

	$cx ="SELECT * from pegawai where username='$username'and password='$password' ";
	$fgt =mysqli_query($conn, $cx);
	$lst =mysqli_num_rows($fgt);

	if($lst>0){
		session_start();
		$d=mysqli_fetch_assoc($fgt);
		$_SESSION['username']=$username;
		$_SESSION['id_pegawai']=$lst['id_pegawai'];
		header("location:../peminjaman/index.php");
	}else{
		header("<script>alert('username atau password salah');location:'login.php'</script>/n");
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Inventaris</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Admin, Dashboard, Bootstrap" />
	<link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">
	
	<link rel="stylesheet" href="../libs/bower/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" href="../libs/bower/animate.css/animate.min.css">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/core.css">
	<link rel="stylesheet" href="../assets/css/misc-pages.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
</head>
<body class="simple-page">
	<div id="back-to-home">
		<a href="index.html" class="btn btn-outline btn-default"><i class="fa fa-home animated zoomIn"></i></a>
	</div>
	<div class="simple-page-wrap">
		<div class="simple-page-logo animated swing">
			<a href="index.html">
				<span><i class="fa fa-gg"></i></span>
				<span>Inventaris</span>
			</a>
		</div><!-- logo -->
		<div class="simple-page-form animated flipInY" id="login-form">
	<h4 class="form-title m-b-xl text-center">PEMINJAMAN</h4>
	<form action="" method="post" id="form1" name="form1">
		<div class="form-group">
			<input id="sign-in-email" type="text" class="form-control" id="username" name="username" placeholder="username">
		</div>

		<div class="form-group">
			<input id="sign-in-password" type="password" class="form-control" id="password" name="password" placeholder="Password">
		</div>

		<div class="form-group m-b-xl">
			<div class="checkbox checkbox-primary">
				<input type="checkbox" id="keep_me_logged_in"/>
				<label for="keep_me_logged_in">Keep me signed in</label>
			</div>
		</div>
		<input type="submit" class="btn btn-primary" name="user_log" value="SING IN">
	</form>
</div><!-- #login-form -->

<div class="simple-page-footer">
	<p><a href="password-forget.php">FORGOT YOUR PASSWORD ?</a></p>
	<p>
		<a href="../login">Kembali</a>
	</p>
</div><!-- .simple-page-footer -->


	</div><!-- .simple-page-wrap -->
</body>
</html>