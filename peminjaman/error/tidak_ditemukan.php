<?php include"header.php"; ?>
<main id="app-main" class="app-main">
  <div class="wrap">
  <section class="app-content">
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
<ol class="breadcrumb">
   
  <li class="breadcrumb-item active">Halaman Tidak Ditemukan</li>
</ol>

<div class="row">
    <div class="col-12">
    	<h1 class="animated bounceInLeft">Halaman Tidak Ditemukan</h1>
        <p class="animated bounceInRight">Halaman yang diminta mungkin tidak ada, atau Anda tidak memiliki akses untuk halaman tersebut.</p>
    </div>
</div>
 </div><!-- .widget -->
      </div><!-- END column -->

    </div><!-- .row -->
  </section><!-- #dash-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
  
  <!-- /#app-footer -->
</main>
<?php include"footer.php" ;?>