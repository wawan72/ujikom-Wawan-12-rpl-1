<?php
  include "header.php";
?>
 <!-- start: Content -->
 <main id="app-main" class="app-main">
  <div class="wrap">
  <section class="app-content">
    <div class="row">
      <!-- DOM dataTable -->
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Default DataTable</h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div class="table-responsive">
              <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Peminjam</th>
                    <th>Tanggal pinjam</th>
                    <th>Tanggal kembali</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
<?php // Load file koneksi.php
  include "koneksi.php";

  $query = "SELECT * FROM peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai order by id_peminjaman desc"; // Query untuk menampilkan semua data siswa
  $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
  $no=1;
  while($data = mysqli_fetch_array($sql)){
?>

          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nama_pegawai']; ?></td>
            <td><?php echo $data['tanggal_peminjaman']; ?></td>
            <td><?php echo $data['tanggal_kembali']; ?></td>
            <td><?php echo $data['status_peminjaman']; ?></td>
            <td>
          <div class="col-md-6">
              <a href="detail_pengembalian.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>" type="button" class="btn btn-3d btn-success">View</a>
          </div>
            </td>
          </tr>
          <?php } ?>
       </tbody>
              </table>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
      
      
    </div><!-- .row -->
  </section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
 
  <!-- /#app-footer -->
</main>
<!-- end: content -->

<?php
  include "footer.php";
?>