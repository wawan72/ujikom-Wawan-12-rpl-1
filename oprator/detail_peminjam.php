<?php
  include "header.php";
?>
 <!-- start: Content -->
<main id="app-main" class="app-main">
  <div class="wrap">
  <section class="app-content">
    <div class="row">
      <!-- DOM dataTable -->
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Default DataTable</h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div class="table-responsive">
              <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>Status</th>
                    <th>pegawai</th>
                  </tr>
                </thead>
                <tbody>
<?php // Load file koneksi.php
  include "koneksi.php";
$id_peminjaman = $_GET ['id_peminjaman'];
  $query = "SELECT * FROM detail_pinjam JOIN inventaris on inventaris.id_inventaris = detail_pinjam.id_inventaris where detail_pinjam.id_peminjaman='$id_peminjaman' ";
  $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
  $no=1;
  while($data = mysqli_fetch_array($sql)){
?>
          <tr>
            <td><?php echo $no++; ?></td>            
            <td><?php echo $data['nama']; ?></td>
            <td><?php echo $data['jmlh']; ?></td>
            <td><?php echo $data['status']; ?></td>
            <td><?php echo $data['id_peminjaman']; ?></td> 
            </td>
          </tr>
          <?php } ?>
      </tbody>
              </table>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
      
      
    </div><!-- .row -->
  </section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
 
  <!-- /#app-footer -->
</main>

<?php
  include "footer.php";
?>