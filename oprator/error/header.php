<?php
session_start();
include"koneksi.php";

  if (!isset($_SESSION['username'])) {

    echo"<script type=text/javascript>
        alert('silahkan login');
        window.location ='../login/index.php';
        </script>";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Admin, Dashboard, Bootstrap" />
	<link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">
	<title>Inventaris</title>
	
	<link rel="stylesheet" href="../../libs/bower/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css">
	<!-- build:css ../assets/css/app.min.css -->
	<link rel="stylesheet" href="../../libs/bower/animate.css/animate.min.css">
	<link rel="stylesheet" href="../../libs/bower/fullcalendar/dist/fullcalendar.min.css">
	<link rel="stylesheet" href="../../libs/bower/perfect-scrollbar/css/perfect-scrollbar.css">
	<link rel="stylesheet" href="../../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../../assets/css/core.css">
	<link rel="stylesheet" href="../../assets/css/app.css">
	<!-- endbuild -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
	<script src="../../libs/bower/breakpoints.js/dist/breakpoints.min.js"></script>
	<script>
		Breakpoints();
	</script>
</head>
	
<body class="menubar-left menubar-unfold menubar-light theme-primary">
<!--============= start main area -->

<!-- APP NAVBAR ==========-->
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top primary">
  
  <!-- navbar header -->
  <div class="navbar-header">
    <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-box"><span class="hamburger-inner"></span></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-more"></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-search"></span>
    </button>

    <a href="../index.html" class="navbar-brand">
      <span class="brand-name">InventarisSKNC</span>
    </a>
  </div><!-- .navbar-header -->
  
  <div class="navbar-container container-fluid">
    <div class="collapse navbar-collapse" id="app-navbar-collapse">
      <ul class="nav navbar-toolbar navbar-toolbar-left navbar-left">
        <li class="hidden-float hidden-menubar-top">
          <a href="javascript:void(0)" role="button" id="menubar-fold-btn" class="hamburger hamburger--arrowalt is-active js-hamburger">
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
          </a>
        </li>
        <li>
          <h5 class="page-title hidden-menubar-top hidden-float">inventaris Sekolah</h5>
        </li>
      </ul>

      <ul class="nav navbar-toolbar navbar-toolbar-right navbar-right">
        <li class="nav-item dropdown hidden-float">
          <a href="javascript:void(0)" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
            <i class="zmdi zmdi-hc-lg zmdi-search"></i>
          </a>
        </li>


       

        <li class="dropdown">
          <a href="javascript:void(0)" class="side-panel-toggle" data-toggle="class" data-target="#side-panel" data-class="open" role="button"><i class="zmdi zmdi-hc-lg zmdi-apps"></i></a>
        </li>
      </ul>
    </div>
  </div><!-- navbar-container -->
</nav>
<!--========== END app navbar -->

<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
  <div class="app-user">

       <?php 
            $use=$_SESSION['username'];
            $fo=mysqli_query($conn,"SELECT img,nama_petugas,nama_level from petugas inner join level on petugas.id_level=level.id_level where username='$use'");
            while($f=mysqli_fetch_array($fo)){
        ?>              

    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md avatar-circle">
          <a href="javascript:void(0)"><img class="img-responsive" src="<?php echo $f['img'];?>" alt="avatar"/></a>
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <h5><a href="javascript:void(0)" class="username"><?php echo $f['nama_petugas']; ?></a></h5>
          <ul>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <small><?php echo $f['nama_level']; ?></small>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu animated flipInY">
                <li>
                  <a class="text-color" href="/index.html">
                    <span class="m-r-xs"><i class="fa fa-home"></i></span>
                    <span>Home</span>
                  </a>
                </li>
                <li>
                  <a class="text-color" href="profile.html">
                    <span class="m-r-xs"><i class="fa fa-user"></i></span>
                    <span>Profile</span>
                  </a>
                </li>
                <li>
                  <a class="text-color" href="settings.html">
                    <span class="m-r-xs"><i class="fa fa-gear"></i></span>
                    <span>Settings</span>
                  </a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                  <a class="text-color" href="../login/logout.php">
                    <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                    <span>Logout bro</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- .media-body -->

    <?php } ?>
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
        

        <li>
          <a href="search.web.html">
            <i class="menu-icon zmdi zmdi-search zmdi-hc-lg"></i>
            <span class="menu-text">Search</span>
          </a>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-pages zmdi-hc-lg"></i>
            <span class="menu-text">pengguna</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
         <ul class="submenu">
            <li><a href="../error/tidak_ditemukan.php"><span class="menu-text">Pegawai</span></a></li>
            <li><a href="../error/tidak_ditemukan.php"><span class="menu-text">Petugas</span></a></li>
        

            </li>
          </ul>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
            <span class="menu-text">Data Tabel</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            <li><a href="../error/tidak_ditemukan.php"><span class="menu-text">Inventaris</span></a></li>
            <li><a href="../error/tidak_ditemukan.php"><span class="menu-text">jenis</span></a></li>
             <li><a href="../error/tidak_ditemukan.php"><span class="menu-text">Ruang</span></a></li>
          </ul>
        </li>
        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
            <span class="menu-text">Transaksi</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            <li><a href="../index.php"><span class="menu-text">Peminjaman</span></a></li>
            <li><a href="../data_pengembalian.php"><span class="menu-text">pengembalian</span></a></li>
          </ul>
        </li>


        <li class="menu-separator"><hr></li>

        <li>
          <a href="../error/tidak_ditemukan.php">
            <i class="menu-icon zmdi zmdi-file-text zmdi-hc-lg"></i>
            <span class="menu-text">Laporan</span>
          </a>
        </li>

       
      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>


<div id="app-customizer" class="app-customizer">
    <a href="javascript:void(0)" 
      class="app-customizer-toggle theme-color" 
      data-toggle="class" 
      data-class="open"
      data-active="false"
      data-target="#app-customizer">
      <i class="fa fa-gear"></i>
    </a>
    <div class="customizer-tabs">
      <!-- tabs list -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#menubar-customizer" aria-controls="menubar-customizer" role="tab" data-toggle="tab">Menubar</a></li>
        <li role="presentation"><a href="#navbar-customizer" aria-controls="navbar-customizer" role="tab" data-toggle="tab">Navbar</a></li>
      </ul><!-- .nav-tabs -->

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane in active fade" id="menubar-customizer">
          <div class="hidden-menubar-top hidden-float">
            <div class="m-b-0">
              <label for="menubar-fold-switch">Fold Menubar</label>
              <div class="pull-right">
                <input id="menubar-fold-switch" type="checkbox" data-switchery data-size="small" />
              </div>
            </div>
            <hr class="m-h-md">
          </div>
          <div class="radio radio-default m-b-md">
            <input type="radio" id="menubar-light-theme" name="menubar-theme" data-toggle="menubar-theme" data-theme="light">
            <label for="menubar-light-theme">Light</label>
          </div>

          <div class="radio radio-inverse m-b-md">
            <input type="radio" id="menubar-dark-theme" name="menubar-theme" data-toggle="menubar-theme" data-theme="dark">
            <label for="menubar-dark-theme">Dark</label>
          </div>
        </div><!-- .tab-pane -->
        <div role="tabpanel" class="tab-pane fade" id="navbar-customizer">
          <!-- This Section is populated Automatically By javascript -->
        </div><!-- .tab-pane -->
      </div>
    </div><!-- .customizer-taps -->
    <hr class="m-0">
    <div class="customizer-reset">
      <button id="customizer-reset-btn" class="btn btn-block btn-outline btn-primary">Reset</button>
      <a href="https://themeforest.net/item/infinity-responsive-web-app-kit/16230780" class="m-t-sm btn btn-block btn-danger">Buy Now</a>
    </div>
  </div><!-- #app-customizer -->
