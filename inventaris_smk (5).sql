-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06 Apr 2019 pada 09.58
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris_smk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jmlh` varchar(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `status` enum('dipinjam','dikembalikan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jmlh`, `id_peminjaman`, `status`) VALUES
(23, 3, '1', 10, 'dikembalikan'),
(24, 3, '2', 11, 'dipinjam'),
(25, 3, '1', 12, 'dipinjam'),
(26, 3, '3', 12, 'dipinjam'),
(27, 3, '2', 13, 'dipinjam'),
(28, 3, '-2', 14, 'dipinjam'),
(29, 3, '12', 14, 'dipinjam'),
(30, 3, '1', 14, 'dipinjam'),
(31, 3, '1', 14, 'dipinjam');

--
-- Trigger `detail_pinjam`
--
DELIMITER $$
CREATE TRIGGER `pengembalian` BEFORE UPDATE ON `detail_pinjam` FOR EACH ROW UPDATE inventaris SET jumlah = jumlah+NEW.jmlh WHERE id_inventaris = NEW.id_inventaris and NEW.status = 'dikembalikan'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `pengurangan_stok` AFTER INSERT ON `detail_pinjam` FOR EACH ROW UPDATE inventaris SET jumlah = jumlah - NEW.jmlh WHERE id_inventaris = new.id_inventaris and new.status = 'dipinjam'
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(3, 'laptop lenovo', 'baik banget', 'okk', 1, 1, '2019-04-03', 10, 'B0001', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` int(11) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'elektronik', 123, 'masook'),
(2, 'bangku', 1232, 'kosong bos'),
(3, 'alat bengkel', 1232, 'ada');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `alamat` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `username`, `password`, `alamat`) VALUES
(1, 'adeR1', 1112222333, 'wawan', 'wwww', 'bogor'),
(2, 'naon we', 222, 'siapa', 'siapa', 'jawa barat'),
(3, 'johan', 14022, 'johan12', 'johan12', 'indonesia'),
(4, 'wawan72', 132580, 'wawan72', 'wawan72', 'semplak bogor'),
(5, 'aku', 111, 'kumah', 'bogor', 'bobolak');

-- --------------------------------------------------------

--
-- Stand-in structure for view `peminjam`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `peminjam` (
`id_inventaris` int(11)
,`id_peminjaman` int(11)
,`nama_pegawai` varchar(100)
,`nama` varchar(100)
,`nama_ruang` varchar(100)
,`jumlah` varchar(11)
,`status_peminjaman` enum('dipinjam','dikembalikan')
,`tanggal_peminjaman` date
,`tanggal_kembali` date
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_peminjaman` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_peminjaman`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(10, '2019-04-03', '2019-04-09', 'dikembalikan', 1),
(11, '2019-04-04', '2019-04-05', 'dipinjam', 4),
(12, '2019-04-04', '2019-04-08', 'dipinjam', 4),
(13, '2019-04-04', '2019-04-22', 'dipinjam', 4),
(14, '2019-04-05', '2019-04-07', 'dipinjam', 4),
(15, '2019-04-06', '2019-04-08', 'dipinjam', 4),
(16, '2019-04-06', '2019-04-08', 'dipinjam', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `id_level` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `baned` enum('N','Y') NOT NULL,
  `logintime` int(2) DEFAULT NULL,
  `img` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'N' COMMENT 'N tidak aktif, Y aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `id_level`, `email`, `baned`, `logintime`, `img`, `status`) VALUES
(1, 'ahmad', 'ahmad', 'usman', 1, 'wawan@gmail.com', 'N', 0, 'images/1.jpg', 'Y'),
(2, 'wawan', 'wawan', 'wawan', 2, 'ahmadsetiyawan@gmail.com', 'N', 0, '', 'N'),
(3, 'username', 'password', 'michael', 2, 'michaeljackson@gmail.com', 'N', NULL, 'images/wolf-hd-wallp', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(11) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(10, 'lab rpl', 'ZP001', 'www');

-- --------------------------------------------------------

--
-- Struktur untuk view `peminjam`
--
DROP TABLE IF EXISTS `peminjam`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `peminjam`  AS  select `c`.`id_inventaris` AS `id_inventaris`,`d`.`id_peminjaman` AS `id_peminjaman`,`a`.`nama_pegawai` AS `nama_pegawai`,`c`.`nama` AS `nama`,`e`.`nama_ruang` AS `nama_ruang`,`b`.`jmlh` AS `jumlah`,`d`.`status_peminjaman` AS `status_peminjaman`,`d`.`tanggal_peminjaman` AS `tanggal_peminjaman`,`d`.`tanggal_kembali` AS `tanggal_kembali` from ((((`pegawai` `a` join `detail_pinjam` `b`) join `inventaris` `c`) join `peminjaman` `d`) join `ruang` `e`) where ((`e`.`id_ruang` = `c`.`id_ruang`) and (`a`.`id_pegawai` = `d`.`id_pegawai`) and (`b`.`id_inventaris` = `c`.`id_inventaris`) and (`b`.`id_peminjaman` = `d`.`id_peminjaman`)) order by `d`.`id_peminjaman` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `id_inventaris` (`id_inventaris`),
  ADD KEY `id_peminjaman` (`id_peminjaman`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
