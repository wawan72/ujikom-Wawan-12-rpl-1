<?php 
include'header.php';
?>
<!--========== END app aside -->

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Inventaris</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Kondisi</th>
										<th>Keterangan</th>
										<th>Jumlah</th>
                    <th>Jenis</th>
										<th>Tanggal Register</th>
                    <th>Ruang</th>
										<th>Kode Inventaris</th>
                    <th>Petugas</th>
                    <th>opsi</th>

									</tr>
								</thead>
                 <a data-toggle="modal" data-target="#myModal"><button type="button" class="btn rounded mw-md btn-primary">Ditambahnya Disini</button></a>
                <br/><br/>
								<?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM inventaris INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis  INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY id_inventaris DESC ");
                while ($d=mysqli_fetch_array($slc)){
                ?>
								<tbody>
								  <tr>
                    <th><?php echo $d['nama']?></th>
                    <th><?php echo $d['kondisi']?></th>
                    <th><?php echo $d['keterangan']?></th>
                    <th><?php echo $d['jumlah']?></th>
                    <th><?php echo $d['nama_jenis']?></th>
                    <th><?php echo $d['tanggal_register']?></th>
                    <th><?php echo $d['nama_ruang']?></th>
                    <th><?php echo $d['kode_inventaris']?></th>
                    <th><?php echo $d['nama_petugas']?></th>
                    <th>
                     <a data-toggle="modal" data-target="#myModal2<?php echo $d['id_inventaris']; ?>" class="fa fa-edit" aria-hidden="true"></i></a>    &nbsp;
                   </th>
                  </tr>
								</tbody>
                <?php
                  }
                ?>
              </table>

              <?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM inventaris");
                while ($d=mysqli_fetch_array($slc)){
                ?>
      <div id="myModal2<?php echo $d['id_inventaris']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit Inventaris</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          </div>
          <div class="modal-body">
            <form action="inventaris_edit.php" method="post" class="form-horizontal">
               <div class="form-group">
                <label for="nama" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <input type="hidden" class="form-control" name="id_inventaris" value="<?php echo $d['id_inventaris'];?>" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama" value="<?php echo $d['nama'];?>" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="kondisi" class="col-sm-2 control-label">Kondisi:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="kondisi" value="<?php echo $d['kondisi'];?>" placeholder="Kondisi">
                </div>
              </div>
               <div class="form-group">
                <label for="keterangan" class="col-sm-2 control-label">Keterangan:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="keterangan" value="<?php echo $d['keterangan'];?>" placeholder="keterangan">
                </div>
              </div>

               <div class="form-group">
                <label for="jumlah" class="col-sm-2 control-label">Jumlah:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="jumlah" value="<?php echo $d['jumlah'];?>" placeholder="Jumlah">
                </div>
              </div>
               <div class="form-group">
                <label for="jumlah" class="col-sm-2 control-label">Tanggal:</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" name="tanggal_register" value="<?php echo $d['tanggal_register'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="jenis" class="col-sm-2 control-label">Jenis:</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="id_jenis" value="<?php echo $d['id_jenis'];?>" placeholder="Jenis" required="">
                </div>
                 <label for="kode" class="col-sm-2 control-label">Kode Inventaris:</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" name="kode_inventaris" value="<?php echo $d['kode_inventaris'];?>"placeholder="Kode Inventaris">
                </div>
              </div>
               <div class="form-group">
                <label for="jumlah" class="col-sm-2 control-label">Petugas:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="id_petugas" value="<?php echo $d['id_petugas'];?>" placeholder="Petugas" required="">
                </div>
              </div>
               <div class="form-group">
                <label for="jumlah" class="col-sm-2 control-label">Ruang:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="id_ruang" value="<?php echo $d['id_ruang'];?>" placeholder="Ruang">
                </div>
              </div>

              
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
    <?php
      }
    ?>
                
					
			<!-- Ajax dataTable -->
		
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
 
  <!-- /#app-footer -->
</main>
<!--========== END app main -->

	<!-- APP CUSTOMIZER -->
	
	<!-- SIDE PANEL -->
	


     <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Tambah Data Inventris</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form action="inventaris_in.php" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama" placeholder="Nama" required="">
                </div>
              </div>
              <div class="form-group">
                <label for="kondisi" class="col-sm-2 control-label">Kondisi:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="kondisi" placeholder="Kondisi" required="">
                </div>
              </div>
               <div class="form-group">
                <label for="jumlah" class="col-sm-2 control-label">Jumlah:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="jumlah" placeholder="Jumlah" required="">
                </div>
              </div>
               <div class="form-group">
                <label for="tanggal_register" class="col-sm-2 control-label">Tanggal:</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" name="tanggal_register" required="">
                </div>
              </div>
              <div class="form-group">
                <?php 
                    include"koneksi.php";
                    $result=mysqli_query($conn,"select * from jenis order by id_jenis asc");
                    $jsArray="var id_jenis = new Array();\n";
                 ?>

                  <label for="jenis" class="col-sm-2 control-label">Jenis:</label>
                <div class="col-sm-3">
                 <select class="form-control" name="id_jenis" onchange="changeValue(this.value)" required=""><option selected="selected" required=""> Jenis 
                    <?php 
                        while ($row = mysqli_fetch_array($result)) {
                          echo "<option value='$row[0].$row[1]'>$row[0].$row[1]</option>";
                          $jsArray .="id_jenis['".$row['id_jenis']."'] = {satu:'".
                              addslashes($row['no'])."'};\n";
                        }
                     ?>

                 </option>
               </select>
                </div>
                
                
                 <label class="col-sm-2 control-label">Kode Inventaris:</label>
                  <div class="col-sm-4">
                    <?php 
                  include"koneksi.php";
                  $cd=mysqli_query($conn,"select max(kode_inventaris) as kode from inventaris");
                  $cari=mysqli_fetch_array($cd);
                  $kode=substr($cari['kode'],1,4);
                  $tambah=$kode+1;

                      if ($tambah<10) {
                        $kode_inventaris="B000".$tambah;
                      }else{
                        $kode_inventaris="B00".$tambah;
                      }
                 ?>
                    <input type="text" class="form-control" name="kode_inventaris" value="<?php echo $kode_inventaris; ?>" required="">
                  </div>
              </div>
               <div class="form-group">
                <label for="jumlah" class="col-sm-2 control-label">Petugas:</label>

                 <?php 
                    include"koneksi.php";
                    $result=mysqli_query($conn,"select * from petugas order by id_petugas asc");
                    $jsArray="var id_petugas = new Array();\n";
                 ?>

                <div class="col-sm-9">
                 <select class="form-control" name="id_petugas" onchange="changeValue(this.value)" required=""><option selected="selected" > petugas 
                    <?php 
                        while ($row = mysqli_fetch_array($result)) {
                          echo "<option value='$row[0].$row[1]'>$row[0].$row[1]</option>";
                          $jsArray .="id_petugas['".$row['id_petugas']."'] = {satu:'".
                              addslashes($row['no'])."'};\n";
                        }
                     ?>

                 </option>
               </select>
                </div>
              </div>
               <div class="form-group">
                <label for="jumlah" class="col-sm-2 control-label">ruang:</label>

                 <?php 
                    include"koneksi.php";
                    $result=mysqli_query($conn,"select * from ruang order by id_ruang asc");
                    $jsArray="var id_ruang = new Array();\n";
                 ?>

                <div class="col-sm-9">
                 <select class="form-control" name="id_ruang" onchange="changeValue(this.value)" required=""><option selected="selected" required=""> ruang 
                    <?php 
                        while ($row = mysqli_fetch_array($result)) {
                          echo "<option value='$row[0].$row[1]'>$row[0].$row[1]</option>";
                          $jsArray .="id_ruang['".$row['id_ruang']."'] = {satu:'".
                              addslashes($row['no'])."'};\n";
                        }
                     ?>

                 </option>
               </select>
                </div>
              </div>

              <div class="form-group">
                <label for="keterangan" class="col-sm-2 control-label">Keterangan:</label>
                <div class="col-sm-9">
                  <input class="form-control" name="keterangan" placeholder="Keterangan" required=""></input>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="checkbox-demo-2"/>
                    <label for="checkbox-demo-2">View my email</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>


	<?php 
include'footer.php';
   ?>