<?php include"header.php"; ?>
<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="excel_inventaris" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Inventaris Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excel_tgl_inventaris.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="excel_inventaris.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="pdf_inventaris" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Inventaris Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdf_tgl_inventaris.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_inventaris.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="excel_pinjam" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Pinjam Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excel_tgl_pinjam.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="excel_pinjam.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="pdf_pinjam" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Pinjam PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdf_tgl_pinjam.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_pinjam.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="excel_kembali" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Kembali Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excel_tgl_kembali.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="excel_kembali.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="pdf_kembali" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Kembali PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdf_tgl_kembali.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_kembali.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="excel_all" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Kembali Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excel_tgl_all.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="excel_all.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="pdf_all" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA Kembali PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdf_tgl_all.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_all.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">inventaris</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body text-center">
						<div class="big-icon m-b-md watermark"><i class="fa fa-5x fa-folder-open"></i></div>
						<h4 class="m-b-md">cetak  laporan Inventaris</h4>
						<p class="text-muted m-b-lg"></p>
						<a data-toggle="modal" data-target="#excel_inventaris" class="btn p-v-xl btn-primary">Excel</a><br><br>
						<a data-toggle="modal" data-target="#pdf_inventaris" class="btn p-v-xl btn-success">PDF</a>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->

			<div class="col-md-3 col-sm-6">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Peminjaman</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body text-center">
						<div class="big-icon m-b-md watermark"><i class="fa fa-5x fa-folder-open"></i></div>
						<h4 class="m-b-md">cetak  laporan Peminjaman</h4>
						<a data-toggle="modal" data-target="#excel_pinjam" class="btn p-v-xl btn-primary">Excel</a><br><br>
						<a data-toggle="modal" data-target="#pdf_pinjam" class="btn p-v-xl btn-success">PDF</a>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
			
			<div class="col-md-3 col-sm-6">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Pengembalian</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body text-center">
						<div class="big-icon m-b-md watermark"><i class="fa fa-5x fa-folder-open"></i></div>
						<h4 class="m-b-md">cetak laporan pengembalian</h4>
						
						<a data-toggle="modal" data-target="#excel_kembali" class="btn p-v-xl btn-primary">Excel</a><br><br>
						<a data-toggle="modal" data-target="#pdf_kembali" class="btn p-v-xl btn-success">PDF</a>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->

			<div class="col-md-3 col-sm-6">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">peminjaman</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body text-center">
						<div class="big-icon m-b-md watermark"><i class="fa fa-5x fa-folder-open"></i></div>
						<h4 class="m-b-md">Cetak laporan Semua Peminjaman</h4>
						
						<a data-toggle="modal" data-target="#excel_all" class="btn p-v-xl btn-primary">Excel</a><br><br>
						<a data-toggle="modal" data-target="#pdf_all" class="btn p-v-xl btn-success">PDF</a>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
			
			<!-- new row -->
			
		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
 
  <!-- /#app-footer -->
</main>
<?php include"footer.php";?>