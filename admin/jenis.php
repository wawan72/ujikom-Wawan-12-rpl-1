<?php 
include'header.php';
?>

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Data jenis</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Kode</th>
                    <th>keterangan</th>
										<th>Opsi</th>
									</tr>
								</thead>
               <a data-toggle="modal" data-target="#myModal"><button type="button" class="btn rounded mw-md btn-primary">Ditambahnya Disini</button></a><br/><br/>
								<?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM jenis");
                while ($d=mysqli_fetch_array($slc)){
                ?>
								<tbody>
								  <tr>
                    <th><?php echo $d['nama_jenis']?></th>
                    <th><?php echo $d['kode_jenis']?></th>
                    <th><?php echo $d['keterangan']?></th>
                    <th>
                      <a data-toggle="modal" data-target="#myModal2<?php echo $d['id_jenis']; ?>" class="fa fa-edit" aria-hidden="true"></a>  &nbsp;
                   
                   </th>
                  </tr>
								</tbody>
                <?php
                  }
                ?>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
			
			<!-- Ajax dataTable -->
		
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
  
  <!-- /#app-footer -->
</main>
<!--========== END app main -->

	<!-- APP CUSTOMIZER -->
	
	<!-- SIDE PANEL -->

  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Tambah Jenis</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form action="jenis_in.php" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_jenis" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="NIP" class="col-sm-2 control-label">Kode:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="kode_jenis" placeholder="kode">
                </div>
              </div>
              
              <div class="form-group">
                <label for="Alamat" class="col-sm-2 control-label">keterangan:</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="keterangan" placeholder="keterangan"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="checkbox-demo-2"/>
                    <label for="checkbox-demo-2">View my email</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
              <?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM jenis");
                while ($d=mysqli_fetch_array($slc)){
              ?>
      <div id="myModal2<?php echo $d['id_jenis']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit jenis</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          </div>
          <div class="modal-body">
            <form action="jenis_edit.php" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <input type="hidden" class="form-control" name="id_jenis" value="<?php echo $d['id_jenis'];?>"  >
                </div>
              </div>
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_jenis" value="<?php echo $d['nama_jenis'];?>"  placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="NIP" class="col-sm-2 control-label">kode:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="kode_jenis" value="<?php echo$d['kode_jenis'];?>" placeholder="kode">
                </div>
              </div>
              
              <div class="form-group">
                <label for="Alamat" class="col-sm-2 control-label">keterangan:</label>
                <div class="col-sm-9">
                  <input class="form-control" name="keterangan" value="<?php echo$d['keterangan'];?>"  placeholder="Alamat">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="checkbox-demo-2"/>
                    <label for="checkbox-demo-2">View my email</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
<?php
}
?>
	<!-- build:js ../assets/js/core.min.js -->
<?php 
include'footer.php';
?>