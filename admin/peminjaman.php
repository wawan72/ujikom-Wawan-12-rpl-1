<?php include"header.php" ?>
<!--========== END app aside -->

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Peminjaman</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>tanggal peminjaman</th>
										<th>tanggal kembali</th>
                    <th>pegawai</th>
                    <th>detail</th>
										<th>Opsi</th>
                   
									</tr>
								</thead>
                <a data-toggle="modal" data-target="#myModal"><button type="button" class="btn rounded mw-md btn-primary">Ditambahnya Disini</button></a>
                <br/><br/>
								<?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM peminjaman join pegawai on peminjaman.id_pegawai = pegawai.id_pegawai");
                while ($d=mysqli_fetch_array($slc)){
                ?>
								<tbody>
								  <tr>
                    <th><?php echo $d['tanggal_peminjaman']?></th>
                    <th><?php echo $d['tanggal_kembali']?></th>
                    <th><?php echo $d['nama_pegawai']?></th>
                    <th>
                      <a href="detail_pinjam.php"><button type="button" class="btn rounded mw-md btn-primary">detail</button></a>
                    </th>
                    <th>
                      <a href=""><i class="fa fa-edit" aria-hidden="true"></i></a>    &nbsp;
                      <a href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                   </th>
                  </tr>
								</tbody>
                <?php
                  }
                ?>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .2idget -->
			</div><!-- END column -->
			
			<!-- Ajax dataTable -->
		
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
  
  <!-- /#app-footer -->
</main>
<!--========== END app main -->

	<!-- APP CUSTOMIZER -->
	
	
	<!-- SIDE PANEL -->
	
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Modal Heading</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="exampleTextInput1" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">Kondisi:</label>
                <div class="col-sm-9">
                  <input type="email" class="form-control" id="email2" placeholder="Kondisi">
                </div>
              </div>
               <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">Jumlah:</label>
                <div class="col-sm-9">
                  <input type="email" class="form-control" id="email2" placeholder="Jumlah">
                </div>
              </div>
              <div class="form-group">
                <label for="textarea1" class="col-sm-2 control-label">Keterangan:</label>
                <div class="col-sm-9">
                  <textarea class="form-control" id="textarea1" placeholder="Keterangan"></textarea>
                </div>
              </div>
             
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
<?php include"footer.php" ?>