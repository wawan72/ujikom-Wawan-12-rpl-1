<?php
include 'koneksi.php';
require('../assets/pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMK NEGERI 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telp : (0251)8631261',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. Raya Laladon Ds.Laladon, Kec.Ciomas Kab.Bogor Kode Pos. 16610',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Email : smkn1_ciomas@yahoo.co.id, Website : www.smkn1ciomas.sch.id',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Inventaris",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Kondisi', 1, 0, 'C'); 
$pdf->Cell(2, 0.8, 'Ket', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Jml', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Jenis', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Ruang', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Kode', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Petugas', 1, 1, 'C'); 
$pdf->SetFont('Arial','',9);
$no=1;

$tanggal_awal = $_POST['tgl_a'];
$tanggal_akhir = $_POST['tgl_b'];

$query=mysqli_query($conn, "SELECT inventaris.*,ruang.nama_ruang,jenis.nama_jenis,petugas.nama_petugas FROM `inventaris` JOIN ruang ON inventaris.id_ruang=ruang.id_ruang JOIN jenis ON jenis.id_jenis=inventaris.id_jenis JOIN petugas ON petugas.id_petugas=inventaris.id_petugas where tanggal_register between '$tanggal_awal' and '$tanggal_akhir' ORDER BY inventaris.id_inventaris DESC");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no++ , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['kondisi'], 1, 0,'C'); 
	$pdf->Cell(2, 0.8, $lihat['keterangan'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['jumlah'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama_jenis'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_register'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama_ruang'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['kode_inventaris'],1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama_petugas'],1, 1, 'C'); 
	
}
	$pdf->Output("laporan_inventaris.pdf","I");

?>

