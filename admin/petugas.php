<?php 
include'header.php';
 ?>
<!--========== END app aside -->

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Petugas</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Username</th>
                    <th>Password</th>
                    <th>email</th>
                    <th>level</th>
                    <th>gambar</th>
                    <th>Setatus</th>
										<th>Opsi</th>
									</tr>
								</thead>
                 <a data-toggle="modal" data-target="#myModal"><button type="button" class="btn rounded mw-md btn-primary">Ditambahnya Disini</button></a>
                <br/><br/>
								<?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM petugas");
                while ($d=mysqli_fetch_array($slc)){
                ?>
								<tbody>
								  <tr>
                    <th><?php echo $d['nama_petugas']?></th>
                    <th><?php echo $d['username']?></th>
                    <th><?php echo $d['password']?></th>
                    <th><?php echo $d['email']?></th>
                    <th><?php echo $d['id_level']?></th>
                    <th><?php echo $d['img']?></th>
                    <th>
                      <?php
                      if($d['status'] == 'Y')
                      {
                        ?>
                      <a href="approve.php?table=petugas&id_petugas=<?php echo $d ['id_petugas']; ?>&action=not-verifed"></br>
                      Aktif
                      </a>
                      <?php
                      }else{
                        ?>
                        
                      <a href="approve.php?table=petugas&id_petugas=<?php echo $d ['id_petugas']; ?>&action=verifed"></br>
                      Tidak Aktif
                      </a>
                      <?php
                      }
                      ?>
                    </th>
                    <th>
                     <a data-toggle="modal" data-target="#myModal2<?php echo $d['id_petugas']; ?>" class="fa fa-edit" aria-hidden="true"></a> &nbsp;
                      
                   </th>
                  </tr>
								</tbody>
                <?php
                  }
                ?>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
			
			<!-- Ajax dataTable -->
		
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
  
</main>
<!--========== END app main -->

	<!-- APP CUSTOMIZER -->
	
	<!-- SIDE PANEL -->
	
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form action="petugas_in.php" method="post" class="form-horizontal" enctype="multipart/form-data" name="form1" id="form1">
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_petugas" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">Username:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
              </div>
               <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">Password:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="password" placeholder="Password">
                </div>
              </div>
               <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">email:</label>
                <div class="col-sm-9">
                  <input type="email" class="form-control" name="email" placeholder="email">
                </div>
              </div>

              <div class="form-group">
                <?php 
                    include"koneksi.php";
                    $result=mysqli_query($conn,"select * from level order by id_level asc");
                    $jsArray="var id_level = new Array();\n";
                 ?>

                  <label for="level" class="col-sm-2 control-label">Level:</label>
                <div class="col-sm-3">
                 <select class="form-control" name="id_level" onchange="changeValue(this.value)" required=""><option selected="selected" required=""> level
                    <?php 
                        while ($row = mysqli_fetch_array($result)) {
                          echo "<option value='$row[0].$row[1]'>$row[0].$row[1]</option>";
                          $jsArray .="id_level['".$row['id_jenis']."'] = {satu:'".
                              addslashes($row['no'])."'};\n";
                        }
                     ?>
                   </option>
                 </select>
               </div>
             </div>
              <div class="form-group">
                <label for="level" class="col-sm-2 control-label"></label>
                  <div class="col-sm-9">
                  <p>Pilih File Gambar :<input name='filegbr' id='Filegambar' type='file'></p>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>

              <?php
                include "koneksi.php"; 
                $slc=mysqli_query($conn,"SELECT * FROM petugas");
                while ($d=mysqli_fetch_array($slc)){
              ?>
     <div id="myModal2<?php echo $d['id_petugas'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit Petugas</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form action="petugas_edit.php" method="post" class="form-horizontal" enctype="multipart/form-data" name="form1" id="form1">
               <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <input type="hidden" class="form-control" name="id_petugas" value="<?php echo $d['id_petugas'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_petugas" value="<?php echo $d['nama_petugas'];?>" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">Username:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="username" value="<?php echo $d['username'];?>" placeholder="Username">
                </div>
              </div>
               <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">Password:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="password" value="<?php echo $d['password'];?>" placeholder="Password">
                </div>
              </div>
                <div class="form-group">
                <label for="level" class="col-sm-2 control-label">email:</label>
                <div class="col-sm-9">
                  <input type="email" class="form-control" name="email" value="<?php echo $d['email'];?>" placeholder="Level">
                </div>
              </div>
              <div class="form-group">
                <label for="level" class="col-sm-2 control-label">level:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="id_level" value="<?php echo $d['id_level'];?>" placeholder="Level">
                </div>
              

              <div class="form-group">
                <label for="level" class="col-sm-2 control-label"></label>
                  <div class="col-sm-9">
                  <img src="<?php echo $d['img'];?>" height="100px">
                  <p>Pilih File Gambar : <br/><input name='filegbr' id='Filegambar' type='file'></p>
              </div>
            </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                 
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
    <?php
  }
    ?>
<?php include 'footer.php'; ?>