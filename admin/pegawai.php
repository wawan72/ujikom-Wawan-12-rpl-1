<?php 
include'header.php';
?>

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Data Pegawai</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Nama</th>
                    <th>NIP</th>
                    <th>username</th>
										<th>password</th>
                    <th>Alamat</th>
										<th>Opsi</th>
									</tr>
								</thead>
               <a data-toggle="modal" data-target="#myModal"><button type="button" class="btn rounded mw-md btn-primary">Ditambahnya Disini</button></a><br/><br/>
								<?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM pegawai");
                while ($d=mysqli_fetch_array($slc)){
                ?>
								<tbody>
								  <tr>
                    <th><?php echo $d['nama_pegawai']?></th>
                    <th><?php echo $d['nip']?></th>
                    <th><?php echo $d['username']?></th>
                    <th><?php echo $d['password']?></th>
                    <th><?php echo $d['alamat']?></th>

                    <th>
                      <a data-toggle="modal" data-target="#myModal2<?php echo $d['id_pegawai']; ?>" class="fa fa-edit" aria-hidden="true"></a>  &nbsp;
                     
                   </th>
                  </tr>
								</tbody>
                <?php
                  }
                ?>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
			
			<!-- Ajax dataTable -->
		
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
 
  <!-- /#app-footer -->
</main>
<!--========== END app main -->

	<!-- APP CUSTOMIZER -->
	
	<!-- SIDE PANEL -->

  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Tambah Data Pegawai</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form action="pegawai_in.php" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_pegawai" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="NIP" class="col-sm-2 control-label">Nip:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nip" placeholder="NIP">
                </div>
              </div>
              <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username:</label>
                <div class="col-sm-9">
                  <input class="form-control" name="username" placeholder="username"></input>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-sm-2 control-label">password:</label>
                <div class="col-sm-9">
                  <input class="form-control" name="password" placeholder="password"></input>
                </div>
              </div>
              <div class="form-group">
                <label for="Alamat" class="col-sm-2 control-label">Alamat:</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="alamat" placeholder="Alamat"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="checkbox-demo-2"/>
                    <label for="checkbox-demo-2">View my email</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
              <?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM pegawai");
                while ($d=mysqli_fetch_array($slc)){
              ?>
      <div id="myModal2<?php echo $d['id_pegawai']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit Pegawai</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          </div>
          <div class="modal-body">
            <form action="pegawai_edit.php" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <input type="hidden" class="form-control" name="id_pegawai" value="<?php echo $d['id_pegawai'];?>"  >
                </div>
              </div>
              <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Nama:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_pegawai" value="<?php echo $d['nama_pegawai'];?>"  placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="NIP" class="col-sm-2 control-label">Nip:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nip" value="<?php echo$d['nip'];?>" placeholder="NIP">
                </div>
              </div>
               <div class="form-group">
                <label for="username" class="col-sm-2 control-label">username:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="username" value="<?php echo$d['username'];?>" placeholder="username">
                </div>
              </div>
               <div class="form-group">
                <label for="password" class="col-sm-2 control-label">password:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="password" value="<?php echo$d['password'];?>" placeholder="password">
                </div>
              </div>
              <div class="form-group">
                <label for="Alamat" class="col-sm-2 control-label">Alamat:</label>
                <div class="col-sm-9">
                  <input class="form-control" name="alamat" value="<?php echo$d['alamat'];?>"  placeholder="Alamat">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="checkbox-demo-2"/>
                    <label for="checkbox-demo-2">View my email</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">simpan</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
<?php
}
?>
	<!-- build:js ../assets/js/core.min.js -->
<?php 
include'footer.php';
?>