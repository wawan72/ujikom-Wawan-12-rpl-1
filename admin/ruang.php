<?php include"header.php"?>
<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
  <div class="wrap">
	<section class="app-content">
		<div class="row">
			<!-- DOM dataTable -->
			<div class="col-md-12">
				<div class="widget">
					<header class="widget-header">
						<h4 class="widget-title">Data Ruangan</h4>
					</header><!-- .widget-header -->
					<hr class="widget-separator">
					<div class="widget-body">
						<div class="table-responsive">
							<table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Nama ruang</th>
										<th>Kode ruang</th>
                    <th>Keterangan</th>
                    <th>Opsi</th>
									</tr>
								</thead>
                 <a data-toggle="modal" data-target="#myModal"><button type="button" class="btn rounded mw-md btn-primary">Ditambahnya Disini</button></a>
                <br/><br/>
								<?php
                include "koneksi.php";
                $slc=mysqli_query($conn,"SELECT * FROM ruang");
                while ($d=mysqli_fetch_array($slc)){
                ?>
								<tbody>
								  <tr>
                    
                    <th><?php echo $d['nama_ruang']?></th>
                    <th><?php echo $d['kode_ruang']?></th>
                    <th><?php echo $d['keterangan']?></th>
                    <th>
                      <a data-toggle="modal" data-target="#ruang<?php echo $d['id_ruang']; ?>" class="fa fa-edit" aria-hidden="true"></a>    &nbsp;
                     
                   </th>
                  </tr>
								</tbody>
                <?php
                  }
                ?>
							</table>
						</div>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
			</div><!-- END column -->
			
			<!-- Ajax dataTable -->
		
		</div><!-- .row -->
	</section><!-- .app-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
  
</main>
<!--========== END app main -->

	<!-- APP CUSTOMIZER -->
	
	<!-- SIDE PANEL -->
	
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Tambah Data Ruangan</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form action="ruang_in.php" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-2 control-label">Nama Ruangan:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_ruang" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                   <?php 
                  include"koneksi.php";
                  $cd=mysqli_query($conn,"select max(kode_ruang) as kode from ruang");
                  $cari=mysqli_fetch_array($cd);
                  $kode=substr($cari['kode'],2,4);
                  $tambah=$kode+1;

                      if ($tambah<10) {
                        $kode_ruang="ZP00".$tambah;
                      }else{
                        $kode_ruang="Z00".$tambah;
                      }
                 ?>
                <label for="email2" class="col-sm-2 control-label">Kode Ruangan:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="kode_ruang" value="<?php echo $kode_ruang;?>" placeholder="Kondisi">
                </div>
              </div>
              <div class="form-group">
                <label for="textarea1" class="col-sm-2 control-label">Keterangan:</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="checkbox-demo-2"/>
                    <label for="checkbox-demo-2">View my email</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">Comment</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
    <?php
      include "koneksi.php";
      $slc=mysqli_query($conn,"SELECT * FROM ruang");
      while ($d=mysqli_fetch_array($slc)){
    ?>
     <div id="ruang<?php echo$d['id_ruang'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit Data Ruangan</h4>
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form action="ruang_edit.php" method="post" class="form-horizontal">
               <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <input type="hidden" class="form-control" name="id_ruang" value="<?php echo $d['id_ruang'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-2 control-label">Nama Ruangan:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_ruang" value="<?php echo $d['nama_ruang'];?>" placeholder="Nama">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-2 control-label">Kode Ruangan:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="kode_ruang" value="<?php echo $d['kode_ruang'];?>" placeholder="Kondisi">
                </div>
              </div>
              <div class="form-group">
                <label for="textarea1" class="col-sm-2 control-label">Keterangan:</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="keterangan" value="<?php echo $d['keterangan'];?>" placeholder="Keterangan"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" id="checkbox-demo-2"/>
                    <label for="checkbox-demo-2">View my email</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button type="submit" class="btn btn-success">Comment</button>
                </div>
              </div>
            </form>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
<?php } ?>
	<?php include"footer.php" ?>