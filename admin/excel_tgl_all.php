<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

  <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Inventaris.xls");
  ?>

  <center>
    <h1>SMK NEGERI 1 CIOMAS</h1><br>
    <h3>Telp : (0251)8631261. JL. Raya Laladon Ds.Laladon, Kec.Ciomas Kab.Bogor Kode Pos. 16610</h3></br>
    <h3>Email : smkn1_ciomas@yahoo.co.id, Website : www.smkn1ciomas.sch.id</h3>
  </center>

  <table border="1">
   <thead>
    <tr> 
                                            <th>No</th>
                                            <th>ID INVENTARIS</th> 
                                            <th>ID PEMINJAMAN</th> 
                                            <th>NAMA PEGAWAI</th> 
                                            <th>NAMA BARANG</th> 
                                            <th>RUANG</th> 
                                            <th>JUMLAH</th> 
                                            <th>STATUS</th> 
                                            <th>Tanggal Pinjam</th>
                                            <th>Tanggal Kembali</th> 
    </tr> 
                                    </thead>
                                    <tbody>
                                   <?php
                          include "koneksi.php";
                          $no=1;  
                          $tanggal_awal = $_POST['tgl_a'];
$tanggal_akhir = $_POST['tgl_b'];
                          $query_mysqli = mysqli_query ($conn,"SELECT * from peminjam where tanggal_register between '$tanggal_awal' and '$tanggal_akhir' ORDER BY id_peminjaman DESC");
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                      <tr>
                        <td><?php echo $no++ ?></td> 
                        <td><?php echo $data['id_inventaris'] ?></td>
                        <td align="center"><?php echo $data['id_peminjaman'] ?></td>
                        <td><?php echo $data['nama_pegawai'] ?></td>
                        <td align="center"><?php echo $data['nama'] ?></td> 
                        <td align="center"><?php echo $data['nama_ruang'] ?></td>  
                        <td align="center"><?php echo $data['jumlah'] ?></td> 
                        <td align="center"><?php echo $data['status_peminjaman'] ?></td>
                        <td align="center"><?php echo $data['tanggal_peminjaman'] ?></td>  
                        <td align="center"><?php echo $data['tanggal_kembali'] ?></td> 
                      
                    </tr>
                      <?php } ?>
                                    </tbody>
                                </table>
                                 
</body>
</html>